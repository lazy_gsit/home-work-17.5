#include <iostream>
#include <math.h>
#include <string>

using namespace std;

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	int VectorLength()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow (z, 2));
	}

private:
	double x;
	double y;
	double z;
};

class AboutMe
{
public:
	AboutMe() : name("Denis "), age(19), hobbies(" games, programming, sport, self-development")
	{}

	void Print()
	{
		cout << "\n" << "My name - " << name << "Age - " << age << " " << "My hobbies - " << hobbies << endl;
	}
private:
	string name;
	string hobbies;
	int age;
};

int main()
{
	Vector v(3, -2, -5);
	cout << "Vector Length - " << v.VectorLength();

	AboutMe aboutMe;
	aboutMe.Print();
}

